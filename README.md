## Практическое задание на React

React-приложение показывающее ближайшие заведения общественного питания c возможностью добавлять новые и управлять информацией о них.

In the project directory, you can run:

### `yarn`

Installing dependencies.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
