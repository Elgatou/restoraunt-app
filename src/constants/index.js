export const DB_URL = "https://places-fdeda.firebaseio.com";
export const API_URL = "https://us-central1-places-fdeda.cloudfunctions.net";
export const KEY = "AIzaSyAXSU4uupM0P15DFa72T1-OvagSEIcR1LY";
export const REFRESH_TOKEN_URL = `https://securetoken.googleapis.com/v1/token?key=${KEY}`;
export const LOGIN_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${KEY}`;
export const REGISTER_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${KEY}`;
