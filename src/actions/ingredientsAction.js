import axios from 'axios';

import { getConfig } from '../utils/axiosConfig';
import { processToken } from '../utils/firebaseAuth';
import { API_URL, DB_URL } from '../constants';

import { GET_INGREDIENTS, CLEAR_INGREDIENTS, CLOSE_MODAL, OPEN_MODAL } from '../actionTypes/actionTypes';

export function getIngredients() {
  return async (dispatch) => {
    const ingredientsObj = await axios.get(`${DB_URL}/ingredients.json`);

    const ingredients = Object.keys(ingredientsObj.data).map((key) => {
      let ingredient = ingredientsObj.data[key];
      ingredient.id = key;
      return ingredient;
    });

    dispatch({
      type: GET_INGREDIENTS,
      payload: ingredients,
    });
  };
}

export function clearIngredients() {
  return { type: CLEAR_INGREDIENTS };
}

export function createIngredient(name, calories) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    await processToken();
    const newIngredient = { name, calories };

    await axios.post(`${API_URL}/app/ingredients`, newIngredient, getConfig());
    const ingredientsObj = await axios.get(`${DB_URL}/ingredients.json`);

    const ingredients = Object.keys(ingredientsObj.data).map((key) => {
      let ingredient = ingredientsObj.data[key];
      ingredient.id = key;
      return ingredient;
    });

    dispatch({
      type: GET_INGREDIENTS,
      payload: ingredients,
    });

    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
    dispatch({ type: CLOSE_MODAL });
  };
}
