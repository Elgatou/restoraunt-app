import { OPEN_MODAL, CLOSE_MODAL } from '../actionTypes/actionTypes';
import { login, logout, register } from '../utils/firebaseAuth';

export function loginAction(email, password, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    const serverResponse = await login(email, password);
    if (serverResponse.status === 200) {
      history.push(`/owner/places`);
    }

    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}

export function logoutAction(history) {
  return async (dispatch) => {
    logout();
    history.push(`/login`);
  };
}

export function registerAction(email, password, history) {
  return async (dispatch) => {
    let serverResponse;

    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });
    serverResponse = await register(email, password);
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });

    if (serverResponse.status === 200) {
      dispatch({
        type: OPEN_MODAL,
        payload: {
          modalType: 'notification',
          modalProps: {
            title: 'Перенаправление',
            text: 'Вы успешно зарегестрировались, сейчас вы будете перенаправлены на страницу входа',
          },
        },
      });

      setTimeout(() => {
        dispatch({
          type: CLOSE_MODAL,
        });
        history.push('/login');
      }, 3000);
    }
  };
}
