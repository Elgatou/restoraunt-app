import axios from 'axios';

import { getConfig } from '../utils/axiosConfig';
import { processToken } from '../utils/firebaseAuth';
import { API_URL, DB_URL } from '../constants';

import { GET_PLACES, CLEAR_EDITED_PLACE, SET_EDITED_PLACE, OPEN_MODAL, CLOSE_MODAL } from '../actionTypes/actionTypes';

export function getPlaces() {
  return async (dispatch) => {
    const placesResponse = await axios.get(`${DB_URL}/places.json`);
    const placesList = placesResponse.data;
    console.log(placesList);

    dispatch({
      type: GET_PLACES,
      payload: placesList,
    });
  };
}

export function setEditedPlace(placeId) {
  return async (dispatch) => {
    const place = await axios.get(`${DB_URL}/places/${placeId}.json`);
    const dishes = await axios.get(`${DB_URL}/dishes.json`);

    console.log(dishes.data);

    const dishesKeys = Object.keys(dishes.data);
    const placeDishesKeys = dishesKeys.filter((dishKey) => dishes.data[dishKey].place === placeId);
    const placeDishes = placeDishesKeys.map((dishKey) => {
      let dish = dishes.data[dishKey];
      dish.id = dishKey;
      return dish;
    });

    const editPlaceData = {
      ...place.data,
      dishes: placeDishes,
    };

    dispatch({
      type: SET_EDITED_PLACE,
      payload: editPlaceData,
    });
  };
}

export function clearEditedPlace() {
  return { type: CLEAR_EDITED_PLACE };
}

export function addPlace(placeObj, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    await processToken();
    const serverResponse = await axios.post(`${API_URL}/app/places`, placeObj, getConfig());

    history.push('/owner/places');
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}

export function editPlace(placeObj, placeId, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    await processToken();
    const serverResponse = await axios.patch(`${API_URL}/app/places/${placeId}`, placeObj, getConfig());

    history.push('/owner/places');
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}

export function deletePlace(placeId, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    await processToken();
    const serverResponse = await axios.delete(`${API_URL}/app/places/${placeId}`, getConfig());

    history.push('/owner/places');
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}
