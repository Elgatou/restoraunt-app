import axios from 'axios';

import { getConfig } from '../utils/axiosConfig';
import { processToken } from '../utils/firebaseAuth';
import { API_URL, DB_URL } from '../constants';

import { SET_EDITED_DISH, CLEAR_EDITED_DISH, OPEN_MODAL, CLOSE_MODAL } from '../actionTypes/actionTypes';

export function setEditedDish(dishId) {
  return async (dispatch) => {
    const dish = await axios.get(`${DB_URL}/dishes/${dishId}.json`);

    dispatch({
      type: SET_EDITED_DISH,
      payload: dish.data,
    });
  };
}

export function clearEditedDish() {
  return { type: CLEAR_EDITED_DISH };
}

export function addDish(dishObj, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    await processToken();
    const serverResponse = await axios.post(`${API_URL}/app/dishes`, dishObj, getConfig());

    history.push(`/owner/places/${dishObj.get('place')}`);
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}

export function editDish(dishObj, dishId, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });
    await processToken();
    const serverResponse = await axios.patch(`${API_URL}/app/dishes/${dishId}`, dishObj, getConfig());

    history.push(`/owner/places/${dishObj.get('place')}`);
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}

export function deleteDish(dishId, placeId, history) {
  return async (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: {
        modalType: 'spinner',
        modalProps: {},
      },
    });

    await processToken();
    const serverResponse = await axios.delete(`${API_URL}/app/dishes/${dishId}`, getConfig());

    history.push(`/owner/places/${placeId}`);
    dispatch({ type: CLOSE_MODAL, payload: 'loaded' });
  };
}
