import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import AuthInput from "../../components/forms/auth-input";

import { connect } from "react-redux";
import { loginAction } from "../../actions/authActions";

import "./login-page.scss";

function LoginPage({ match, history, common, loginAction }) {
  if (localStorage.getItem("token") !== null) history.push("/owner/places");

  const [login, setLogin] = useState("");
  const [loginValid, setLoginValid] = useState(true);
  const [password, setPassword] = useState("");
  const [passwordValid, setPasswordValid] = useState(true);

  function validate() {
    let valid = true;
    if (login.match(/[^/w|/+|/-|/@|/.]/) || login.length === 0) {
      setLoginValid(false);
      valid = false;
    }
    if (password.match(/[^/w|/+|/-|/@|/.]/) || password.length === 0) {
      setPasswordValid(false);
      valid = false;
    }
    return valid;
  }

  function handleLogin(event) {
    event.preventDefault();
    const valid = validate();
    if (valid && !common.loading) loginAction(login, password, history);
  }

  return (
    <form onSubmit={event => handleLogin(event)} className="login-page">
      <h1>Вход в систему</h1>
      <div className="login-page__field">
        <AuthInput
          type={"text"}
          placeholder={"email"}
          value={login}
          setValue={setLogin}
          valid={loginValid}
          setValid={setLoginValid}
          validationErrorMessage={
            "обязательное поле, Только буквы, цифры и символы @.+-_"
          }
        />
      </div>
      <div className="login-page__field">
        <AuthInput
          type={"password"}
          placeholder={"Пароль"}
          value={password}
          setValue={setPassword}
          valid={passwordValid}
          setValid={setPasswordValid}
          validationErrorMessage={
            "обязательное поле, Только буквы, цифры и символы @.+-_"
          }
        />
      </div>

      <div className="login-page__buttons">
        <Button type="submit">Войти</Button>
        <Link className="login-page__link" to="/register">
          Регистрация
        </Link>
      </div>
    </form>
  );
}

const mapStateToProps = store => {
  return {
    common: store.common
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginAction: (login, password, history) =>
      dispatch(loginAction(login, password, history))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
