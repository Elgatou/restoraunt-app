import React, { useEffect } from "react";
import Navbar from "../../../components/navbar";
import MyPlaces from "../../../components/my-places";

import "./places.scss";
import FormPageWrapper from "../../../components/forms/form-page-wrapper/form-page-wrapper";

import { connect } from "react-redux";
import { getPlaces } from "../../../actions/placesActions";

function Places({ places, getPlaces, logoutAction, history }) {
  if (localStorage.getItem("token") === null) history.push("/login");

  useEffect(() => {
    getPlaces();
  }, [getPlaces]);

  return (
    <>
      <Navbar />
      <FormPageWrapper>
        <MyPlaces listObject={places.list} />
      </FormPageWrapper>
    </>
  );
}

const mapStateToProps = store => {
  return {
    places: store.places
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPlaces: () => dispatch(getPlaces())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Places);
