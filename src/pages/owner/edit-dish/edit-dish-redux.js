import React, { useState, useEffect } from "react";

import FormPageWrapper from "../../../components/forms/form-page-wrapper/form-page-wrapper";
import Navbar from "../../../components/navbar";
import Title from "../../../components/forms/title";
import Photo from "../../../components/forms/photo";
import Price from "../../../components/forms/price";
import Ingredients from "../../../components/forms/ingredients";
import ManageButtons from "../../../components/forms/manage-buttons";

import { connect } from "react-redux";

import {
  setEditedDish,
  clearEditedDish,
  addDish,
  deleteDish,
  editDish
} from "../../../actions/dishActions";
import { openModal, closeModal } from "../../../actions/modalActions";

function EditDish({
  match,
  history,
  common,
  dish,
  setEditedDish,
  clearEditedDish,
  addDish,
  deleteDish,
  editDish,
  openModal,
  closeModal
}) {
  if (localStorage.getItem("token") === null) history.push("/login");

  const [title, setTitle] = useState("");
  const [titleIsValid, setTitleIsValid] = useState(true);

  const [photo, setPhoto] = useState(null);
  const [photoPreview, setPhotoPreview] = useState(null);
  const [photoIsValid, setPhotoIsValid] = useState(true);

  const [price, setPrice] = useState(0);

  const [ingredients, setIngredients] = useState([]);
  const [ingredientsIsValid, setIngredientsIsValid] = useState(true);

  //call when mounting
  useEffect(() => {
    if (match.params.dishId !== "add") {
      setEditedDish(match.params.dishId);

      return () => clearEditedDish();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (dish.editedDish && match.params.dishId !== "add") {
      const { image, name, price, ingredients } = dish.editedDish;

      setTitle(name);
      setPhotoPreview(image);
      setPhotoIsValid(true);
      setPrice(price);
      setIngredients(ingredients);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dish.editedDish]);

  function validateForm(formType) {
    let isValid = title && titleIsValid && ingredients.length;

    if (formType === "add") isValid = isValid && photo && photoIsValid;
    return isValid;
  }

  function createFormDataWithourPhoto() {
    let dish = {};
    const dishFormData = new FormData();
    dishFormData.set("name", title);

    dishFormData.set("price", price);
    dishFormData.set("place", match.params.id);

    dish.formData = dishFormData;

    for (var i = 0; i < ingredients.length; i++) {
      dishFormData.append("ingredients", ingredients[i].id);
    }
    return dishFormData;
  }

  function setValidateErrors(formType) {
    if (!title) setTitleIsValid(false);
    if (!ingredients.length) setIngredientsIsValid(false);

    if (formType === "add") {
      if (!photo) setPhotoIsValid(false);
    }
  }

  function addDishHandler(event) {
    event.preventDefault();
    const formIsValid = validateForm("add");

    if (formIsValid && !common.loading) {
      const dishFormData = createFormDataWithourPhoto();
      dishFormData.set("photo", photo);
      addDish(dishFormData, history);
    } else setValidateErrors("add");
  }

  function editDishHandler(event, dishId) {
    event.preventDefault();
    const formIsValid = validateForm("edit");

    if (formIsValid && !common.loading) {
      const dishFormData = createFormDataWithourPhoto();
      if (photo && photoIsValid) dishFormData.set("photo", photo);

      editDish(dishFormData, dishId, history);
    } else setValidateErrors("edit");
  }

  function deleteDishHandler(event, dishId) {
    event.preventDefault();
    openModal("confirm", {
      text: "Вы действительно хотите удалить блюдо?",
      confirmAction: () => {
        closeModal();
        deleteDish(dishId, match.params.id, history);
      }
    });
  }

  return (
    <>
      <Navbar />
      <FormPageWrapper>
        <h1>{title ? title : "Новое блюдо"}</h1>
        <form>
          <Title
            title={title}
            setTitle={setTitle}
            titleIsValid={titleIsValid}
            setTitleIsValid={setTitleIsValid}
          />
          <Photo
            photo={photo}
            setPhoto={setPhoto}
            photoPreview={photoPreview}
            setPhotoPreview={setPhotoPreview}
            photoIsValid={photoIsValid}
            setPhotoIsValid={setPhotoIsValid}
          />
          <Price price={price} setPrice={setPrice} />
          <Ingredients
            ingredients={ingredients}
            setIngredients={setIngredients}
            ingredientsIsValid={ingredientsIsValid}
            setIngredientsIsValid={setIngredientsIsValid}
            openModal={openModal}
          />
          <ManageButtons
            addHandler={addDishHandler}
            deleteHandler={deleteDishHandler}
            editHandler={editDishHandler}
            id={match.params.dishId}
            type={"блюдо"}
          />
        </form>
      </FormPageWrapper>
    </>
  );
}

const mapStateToProps = store => {
  return {
    common: store.common,
    dish: store.dish
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setEditedDish: dishId => dispatch(setEditedDish(dishId)),
    clearEditedDish: () => dispatch(clearEditedDish()),
    addDish: (dishObj, history) => dispatch(addDish(dishObj, history)),
    editDish: (dishObj, dishId, history) =>
      dispatch(editDish(dishObj, dishId, history)),
    deleteDish: (dishId, placeId, history) =>
      dispatch(deleteDish(dishId, placeId, history)),
    openModal: (modalType, modalProps) =>
      dispatch(openModal(modalType, modalProps)),
    closeModal: () => dispatch(closeModal())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditDish);
