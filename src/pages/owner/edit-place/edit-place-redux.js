import React, { useState, useEffect } from "react";

import "./edit-place.scss";

import FormPageWrapper from "../../../components/forms/form-page-wrapper/form-page-wrapper";
import Navbar from "../../../components/navbar";
import Title from "../../../components/forms/title";
import Photo from "../../../components/forms/photo";
import Hours from "../../../components/forms/hours";
import Address from "../../../components/forms/address";
import Dishes from "../../../components/forms/dishes";
import ManageButtons from "../../../components/forms/manage-buttons";

import { connect } from "react-redux";

import {
  setEditedPlace,
  clearEditedPlace,
  addPlace,
  deletePlace,
  editPlace
} from "../../../actions/placesActions";
import { openModal, closeModal } from "../../../actions/modalActions";

function EditPlaces({
  match,
  history,
  common,
  places,
  setEditedPlace,
  clearEditedPlace,
  addPlace,
  deletePlace,
  editPlace,
  openModal,
  closeModal
}) {
  if (localStorage.getItem("token") === null) history.push("/login");

  const [title, setTitle] = useState("");
  const [titleIsValid, setTitleIsValid] = useState(true);

  const [photo, setPhoto] = useState(null);
  const [photoPreview, setPhotoPreview] = useState(null);
  const [photoIsValid, setPhotoIsValid] = useState(true);

  const [openHours, setOpenHours] = useState("");
  const [closeHours, setCloseHours] = useState("");
  const [hoursIsValid, setHoursIsValid] = useState(true);

  const [address, setAddress] = useState("");
  const [addressIsValid, setAddressIsValid] = useState(true);

  const [dishes, setDishes] = useState([]);

  //call when mounting
  useEffect(() => {
    if (match.params.id !== "add") {
      setEditedPlace(match.params.id);

      return () => clearEditedPlace();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (places.editedPlace && match.params.id !== "add") {
      const {
        address,
        image,
        name,
        to_hour,
        from_hour,
        dishes
      } = places.editedPlace;

      setTitle(name);
      setPhotoPreview(image);
      setPhotoIsValid(true);
      setOpenHours(from_hour);
      setCloseHours(to_hour);
      setAddress(address);
      setDishes(dishes);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [places.editedPlace]);

  function validateForm(formType) {
    let isValid =
      address &&
      openHours &&
      closeHours &&
      title &&
      titleIsValid &&
      hoursIsValid;

    if (formType === "add") isValid = isValid && photo && photoIsValid;
    return isValid;
  }

  function createFormDataWithourPhoto() {
    const placeFormData = new FormData();
    placeFormData.set("name", title);

    placeFormData.set("from_hour", openHours);
    placeFormData.set("to_hour", closeHours);
    placeFormData.set("address", address);
    return placeFormData;
  }

  function setValidateErrors(formType) {
    if (!title) setTitleIsValid(false);
    if (!address) setAddressIsValid(false);
    if (!openHours || !closeHours || openHours > closeHours)
      setHoursIsValid(false);

    if (formType === "add") {
      if (!photo) setPhotoIsValid(false);
    }
  }

  function addPlaceHandler(event) {
    event.preventDefault();
    const formIsValid = validateForm("add");

    if (formIsValid && !common.loading) {
      const placeFormData = createFormDataWithourPhoto();
      placeFormData.set("image", photo);
      addPlace(placeFormData, history);
    } else setValidateErrors("add");
  }

  function editPlaceHandler(event, placeId) {
    event.preventDefault();
    const formIsValid = validateForm("edit");

    if (formIsValid && !common.loading) {
      const placeFormData = createFormDataWithourPhoto();
      if (photo && photoIsValid) placeFormData.set("image", photo);
      editPlace(placeFormData, placeId, history);
    } else setValidateErrors("edit");
  }

  function deletePlaceHandler(event, placeId) {
    event.preventDefault();
    openModal("confirm", {
      text: "Вы действительно хотите удалить Заведение?",
      confirmAction: () => {
        closeModal();
        deletePlace(placeId, history);
      }
    });
  }

  return (
    <>
      <Navbar />
      <FormPageWrapper>
        <h1>{title ? title : "Новое заведение"}</h1>
        <form>
          <Title
            title={title}
            setTitle={setTitle}
            titleIsValid={titleIsValid}
            setTitleIsValid={setTitleIsValid}
          />
          <Photo
            photo={photo}
            setPhoto={setPhoto}
            photoPreview={photoPreview}
            setPhotoPreview={setPhotoPreview}
            photoIsValid={photoIsValid}
            setPhotoIsValid={setPhotoIsValid}
          />
          <Hours
            openHours={openHours}
            closeHours={closeHours}
            onChangeOpenHours={setOpenHours}
            onChangeCloseHours={setCloseHours}
            hoursIsValid={hoursIsValid}
            setHoursIsValid={setHoursIsValid}
          />
          <Address
            address={address}
            setAddress={setAddress}
            addressIsValid={addressIsValid}
            setAddressIsValid={setAddressIsValid}
          />

          {match.params.id !== "add" && (
            <Dishes dishes={dishes} match={match} />
          )}

          <ManageButtons
            addHandler={addPlaceHandler}
            deleteHandler={deletePlaceHandler}
            editHandler={editPlaceHandler}
            id={match.params.id}
          />
        </form>
      </FormPageWrapper>
    </>
  );
}

const mapStateToProps = store => {
  return {
    common: store.common,
    places: store.places
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setEditedPlace: placeId => dispatch(setEditedPlace(placeId)),
    clearEditedPlace: () => dispatch(clearEditedPlace()),
    addPlace: (placeObj, history) => dispatch(addPlace(placeObj, history)),
    editPlace: (placeObj, placeId, history) =>
      dispatch(editPlace(placeObj, placeId, history)),
    deletePlace: (placeId, history) => dispatch(deletePlace(placeId, history)),
    openModal: (modalType, modalProps) =>
      dispatch(openModal(modalType, modalProps)),
    closeModal: () => dispatch(closeModal())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPlaces);
