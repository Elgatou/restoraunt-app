import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import AuthInput from "../../components/forms/auth-input";

import { connect } from "react-redux";
import { registerAction } from "../../actions/authActions";

import "./register-page.scss";

function RegisterPage({ match, history, common, registerAction }) {
  if (localStorage.getItem("token") !== null) history.push("/owner/places");

  const [login, setLogin] = useState("");
  const [loginValid, setLoginValid] = useState(true);

  const [password, setPassword] = useState("");
  const [passwordValid, setPasswordValid] = useState(true);

  const [passwordCheck, setPasswordCheck] = useState("");
  const [passwordCheckValid, setPasswordCheckValid] = useState(true);

  function validate() {
    let valid = true;
    if (login.match(/[^/w|/+|/-|/@|/.]/) || login.length === 0) {
      setLoginValid(false);
      valid = false;
    }
    if (
      password.match(/[^/w|/+|/-|/@|/.]/) ||
      password.length === 0 ||
      password.length < 6
    ) {
      setPasswordValid(false);
      valid = false;
    }
    if (password !== passwordCheck || password.length === 0) {
      setPasswordCheckValid(false);
      valid = false;
    }

    if (!valid) console.log(false);
    return valid;
  }

  function handleLogin(event) {
    event.preventDefault();
    const valid = validate();
    if (valid && !common.loading) registerAction(login, password, history);
  }

  return (
    <form onSubmit={event => handleLogin(event)} className="register-page">
      <h1>Регистрация в системе</h1>
      <div className="login-page__field">
        <AuthInput
          type={"text"}
          placeholder={"email"}
          value={login}
          setValue={setLogin}
          valid={loginValid}
          setValid={setLoginValid}
          validationErrorMessage={
            "обязательное поле, Только буквы, цифры и символы @.+-_"
          }
        />
      </div>
      <div className="login-page__field">
        <AuthInput
          type={"password"}
          placeholder={"Пароль"}
          value={password}
          setValue={setPassword}
          valid={passwordValid}
          setValid={setPasswordValid}
          validationErrorMessage={
            "обязательное поле, минимальное количество символов - 6 Только буквы, цифры и символы @.+-_"
          }
        />
      </div>
      <div className="login-page__field">
        <AuthInput
          type={"password"}
          placeholder={"Подтверждение пароля"}
          value={passwordCheck}
          setValue={setPasswordCheck}
          valid={passwordCheckValid}
          setValid={setPasswordCheckValid}
          validationErrorMessage={"Обязательное поле, пароли должны совпадать"}
        />
      </div>
      <div className="register-page__buttons">
        <Button>Зарегестрироваться</Button>
        <Link className="register-page__link" to="/login">
          Вход
        </Link>
      </div>
    </form>
  );
}

const mapStateToProps = store => {
  return {
    common: store.common
  };
};

const mapDispatchToProps = dispatch => {
  return {
    registerAction: (login, password, history) =>
      dispatch(registerAction(login, password, history))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPage);
