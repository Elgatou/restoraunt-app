import { browserHistory } from "react-router-dom";

import { ROUTING } from "../actionTypes/Routing";

export const redirect = store => next => action => {
  if (action.type === ROUTING) {
    browserHistory[action.payload.method](action.payload.nextUrl);
  }

  return next(action);
};
