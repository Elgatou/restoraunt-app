import React from "react";
import noImage from "./no-image.png";
import { Label } from "semantic-ui-react";

import "../form-field.scss";
import "./photo.scss";
function Photo({
  setPhoto,
  photoPreview,
  setPhotoPreview,
  photoIsValid,
  setPhotoIsValid
}) {
  function generatePreviewImgUrl(file) {
    const reader = new FileReader();
    const url = reader.readAsDataURL(file);
    reader.onloadend = e => setPhotoPreview(reader.result);
  }

  function onPhotoChange(file) {
    if (!file) {
      return;
    }

    if (file.size > 1048576) setPhotoIsValid(false);
    else setPhotoIsValid(true);

    setPhoto(file);
    generatePreviewImgUrl(file);
  }

  const photoSrc = photoPreview ? photoPreview : noImage;

  return (
    <div className="form-field">
      <span className="form-field__label">Фотография:</span>
      <div className="form-field__group">
        <input
          className="form-field__photo-upload-input"
          type="file"
          id="photo-upload"
          accept="image/*"
          onChange={e => {
            onPhotoChange(e.target.files[0]);
          }}
        />
        <img alt="Фото" src={photoSrc} width="200" height="200" />

        <label className="form-field__custom-button" htmlFor="photo-upload">
          Добавить/Изменить фотографию
        </label>
      </div>
      {!photoIsValid && (
        <Label basic color="red" pointing>
          Обязательное поле при создании заведения, размер фотографии не должен
          превышать 1 мб
        </Label>
      )}
    </div>
  );
}

export default Photo;
