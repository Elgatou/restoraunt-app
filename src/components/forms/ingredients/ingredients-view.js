import React from "react";
import { Label } from "semantic-ui-react";
import "../form-field.scss";
import "./ingredients.scss";

function ingredients({
  ingredients,
  setIngredients,
  ingredientsIsValid,
  setIngredientsIsValid,
  openModal
}) {
  function handleIngredientClick(event, ingredientId) {
    event.preventDefault();
    const filteredIngridients = ingredients.filter(
      elem => elem.id !== ingredientId
    );
    setIngredients(filteredIngridients);
  }

  const ingredientsList = ingredients.map((elem, index) => {
    return (
      <li key={elem.id}>
        <a
          onClick={event => handleIngredientClick(event, elem.id)}
          className="ingredients__element"
          href=""
        >
          {elem.name}
        </a>
      </li>
    );
  });

  return (
    <div className="form-field">
      <span className="form-field__label">ингредиенты:</span>
      <div className=" form-field__group ingredients">
        <ul className="ingredients__list">{ingredientsList}</ul>
        <button
          type="button"
          className="form-field__custom-button"
          onClick={e => {
            e.preventDefault();
            openModal("ingredientsSelect", {
              selectedIngredientsArray: ingredients,
              setIngredients,
              setIngredientsIsValid
            });
          }}
        >
          Добавить ингредиент
        </button>
      </div>
      {!ingredientsIsValid && (
        <Label basic color="red">
          Добавьте ингредиенты
        </Label>
      )}
    </div>
  );
}

export default ingredients;
