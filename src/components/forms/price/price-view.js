import React from "react";

import "../form-field.scss";

function Price({ price, setPrice }) {
  return (
    <div className="form-field">
      <label className="form-field__label" htmlFor="price">
        Цена:
      </label>
      <div className="ui input form-field__group">
        <input
          value={price}
          name="price"
          id="price"
          type="number"
          onChange={e => setPrice(e.target.value)}
        />
      </div>
    </div>
  );
}

export default Price;
