import React from "react";
import { Label, Input } from "semantic-ui-react";
import "./auth-input.scss";

function AuthInput({
  type,
  placeholder,
  value,
  setValue,
  valid,
  setValid,
  validationErrorMessage
}) {
  function onInputChange(value) {
    if (value.length !== 0) {
      setValid(true);
    }
    setValue(value);
  }

  return (
    <>
      <Input
        value={value}
        type={type}
        className="auth-input"
        onChange={e => onInputChange(e.target.value)}
        placeholder={placeholder}
      />
      {!valid && (
        <Label basic color="red" pointing>
          {validationErrorMessage}
        </Label>
      )}
    </>
  );
}

export default AuthInput;
