import React from "react";
import "./form-page-wrapper.scss";

const FormPageWrapper = ({ children }) => {
  return (
    <>
      <div className="form-page-wrapper">{children}</div>
    </>
  );
};

export default FormPageWrapper;
