import React from "react";
import { Label } from "semantic-ui-react";
import "../form-field.scss";

function Title({ title, setTitle, titleIsValid, setTitleIsValid }) {
  function onTitleChange(value) {
    if (value.length === 0 || value.length > 20) {
      setTitleIsValid(false);
    } else {
      setTitleIsValid(true);
    }
    setTitle(value);
  }

  return (
    <div className="form-field">
      <label className="form-field__label" htmlFor="title">
        Название:
      </label>
      <div className="ui input form-field__group">
        <input
          value={title}
          required
          name="title"
          id="title"
          type="text"
          onChange={e => onTitleChange(e.target.value)}
        />
      </div>
      {!titleIsValid && (
        <Label basic color="red" pointing>
          Поле "Название" не может быть пустым и не должно содержать больше 20
          символов
        </Label>
      )}
    </div>
  );
}

export default Title;
