import React from "react";
import { Label } from "semantic-ui-react";
import "../form-field.scss";

function Address({ address, setAddress, addressIsValid, setAddressIsValid }) {
  function onAdressChange(value) {
    if (value.length === 0) {
      setAddressIsValid(false);
    } else {
      setAddressIsValid(true);
    }
    setAddress(value);
  }

  return (
    <div className="form-field">
      <label className="form-field__label" htmlFor="address">
        Адрес:
      </label>
      <div className="ui input form-field__group">
        <input
          required
          name="address"
          id="address"
          type="text"
          value={address}
          onChange={e => onAdressChange(e.target.value)}
        />
      </div>
      {!addressIsValid && (
        <Label basic color="red" pointing>
          Укажите корректный адрес
        </Label>
      )}
    </div>
  );
}

export default Address;
