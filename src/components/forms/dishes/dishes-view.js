import React from "react";
import { Link } from "react-router-dom";
import "../form-field.scss";
import "./dishes.scss";

function Dishes({ dishes, match }) {
  const dishesList = dishes.map(elem => {
    return (
      <li key={elem.id}>
        <Link className="dishes__element" to={`${match.url}/dishes/${elem.id}`}>
          {elem.name}
        </Link>
      </li>
    );
  });

  return (
    <div className="form-field">
      <span className="form-field__label">Блюда:</span>
      <div className=" form-field__group dishes">
        <ul className="dishes__list">{dishesList}</ul>
        <Link to={`${match.url}/dishes/add`}>
          <button type="button" className="form-field__custom-button">
            Добавить блюдо
          </button>
        </Link>
      </div>
    </div>
  );
}

export default Dishes;
