import React, { useEffect } from "react";
import { Label } from "semantic-ui-react";
import "../form-field.scss";

function Hours({
  openHours,
  onChangeOpenHours,
  closeHours,
  onChangeCloseHours,
  hoursIsValid,
  setHoursIsValid
}) {
  useEffect(() => {
    if (openHours > closeHours) setHoursIsValid(false);
    else setHoursIsValid(true);
  }, [closeHours, openHours, setHoursIsValid]);

  return (
    <div className="form-field">
      <span className="form-field__label">Режим работы:</span>
      <div className="ui input form-field__group form-field__group_hours">
        <label
          className="form-field__label form-field__label_hours"
          htmlFor="open-hours"
        >
          с
        </label>
        <input
          required
          className="form-field__input form-field__input_hours"
          name="open-hours"
          id="open-hours"
          placeholder="с"
          type="time"
          value={openHours}
          onChange={e => onChangeOpenHours(e.target.value)}
        />
        <label
          className="form-field__label form-field__label_hours"
          htmlFor="open-hours"
        >
          до
        </label>
        <input
          required
          className="form-field__input form-field__input_hours"
          name="close-hours"
          id="close-hours"
          placeholder="до"
          type="time"
          value={closeHours}
          onChange={e => onChangeCloseHours(e.target.value)}
        />
      </div>
      {!hoursIsValid && (
        <Label basic color="red" pointing>
          Обязательное поле, время открытия должно быть меньше времени закрытия
        </Label>
      )}
    </div>
  );
}

export default Hours;
