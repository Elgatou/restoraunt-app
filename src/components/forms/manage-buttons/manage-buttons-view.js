import React from "react";
import "../form-field.scss";

function ManageButtons({ id, addHandler, deleteHandler, editHandler, type }) {
  const addButtons = (
    <>
      <button
        onClick={e => addHandler(e)}
        className="form-field__custom-button"
      >
        Добавить {type}
      </button>
    </>
  );

  const editButtons = (
    <>
      <button
        onClick={event => editHandler(event, id)}
        className="form-field__custom-button"
      >
        Сохранить
      </button>
      <button
        onClick={event => deleteHandler(event, id)}
        className="form-field__custom-button form-field__custom-button_danger"
      >
        Удалить {type}
      </button>
    </>
  );

  return (
    <div className="form-field">
      <div className="form-field__group form-field__group_manage-buttons">
        {id === "add" ? addButtons : editButtons}
      </div>
    </div>
  );
}

export default ManageButtons;
