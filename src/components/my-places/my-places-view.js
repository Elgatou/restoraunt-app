import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

import "./my-places.scss";

function MyPlaces({ listObject }) {
  const listKeys = Object.keys(listObject);

  const placesList = listKeys.map(elem => {
    return (
      <li key={elem}>
        <Link to={`/owner/places/${elem}`}>{listObject[elem].name}</Link>
      </li>
    );
  });

  return (
    <div className="my-places">
      <h1>Мои заведения</h1>
      <ul className="my-places__list">{placesList}</ul>
      <Link to="/owner/places/add">
        <Button>Добавить заведение</Button>
      </Link>
    </div>
  );
}

export default MyPlaces;
