import React from "react";
import "./modal-wrapper.scss";

const Modal = ({ children, z, transparent }) => {
  return (
    <>
      <div
        style={{ zIndex: z + 5 }}
        className={
          transparent
            ? "modal__container modal__container--transparent"
            : "modal__container"
        }
      >
        {children}
      </div>
      <div style={{ zIndex: z }} className="modal__backdrop" />
    </>
  );
};

export default Modal;
