import React from "react";
import { connect } from "react-redux";

import { closeModal, openModal } from "../../../actions/modalActions";

import Confirm from "../confirm";
import Notification from "../notification";
import ModalWrapper from "../modal-wrapper";
import IngredientsSelect from "../ingredients-select";
import AddIngredient from "../add-ingredient";
import Spinner from "../spinner";

const modalComponentLookupTable = {
  notification: Notification,
  confirm: Confirm,
  ingredientsSelect: IngredientsSelect,
  addIngredient: AddIngredient,
  spinner: Spinner
};

function ModalManager({ modalDescriptionList, closeModal, openModal }) {
  const renderedModals = modalDescriptionList.map((modalDescription, index) => {
    const { modalType, modalProps = {} } = modalDescription;

    const ModalComponent = modalComponentLookupTable[modalType];
    return (
      <ModalWrapper
        transparent={modalType === "spinner" ? true : false}
        z={(index + 1) * 10}
        key={modalType + index}
      >
        <ModalComponent
          {...modalProps}
          closeModal={closeModal}
          openModal={openModal}
        />
      </ModalWrapper>
    );
  });

  return renderedModals;
}

const mapStateToProps = store => {
  return {
    modalDescriptionList: store.modal
  };
};

const mapDispatchToProps = dispatch => {
  return {
    closeModal: () => dispatch(closeModal()),
    openModal: () => dispatch(openModal())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalManager);
