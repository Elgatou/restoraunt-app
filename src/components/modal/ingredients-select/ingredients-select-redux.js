import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Icon } from "semantic-ui-react";
import "./ingredients-select.scss";
import "../modal.scss";

import { closeModal, openModal } from "../../../actions/modalActions";

import {
  getIngredients,
  clearIngredients,
  createIngredient
} from "../../../actions/ingredientsAction";

const IngredientsSelect = ({
  selectedIngredientsArray,
  setIngredients,
  setIngredientsIsValid,
  ingredients,
  getIngredients,
  clearIngredients,
  createIngredient,
  closeModal,
  openModal
}) => {
  const [selectedIngredients, setSelectedIngredients] = useState(null);

  useEffect(() => {
    getIngredients();

    const selectedIngredientsMap = new Map();
    selectedIngredientsArray.forEach(element => {
      selectedIngredientsMap.set(element.id, element);
    });
    setSelectedIngredients(selectedIngredientsMap);

    return () => clearIngredients();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderedIngredients = ingredients.map(ingredient => {
    const cssClass = selectedIngredients.get(ingredient.id)
      ? "ingredients-select__list-element ingredients-select__list-element_selected"
      : "ingredients-select__list-element";
    return (
      <li
        onClick={() =>
          toggleIngredient(ingredient.id, ingredient.name, ingredient.calories)
        }
        className={cssClass}
        key={ingredient.id}
      >
        {ingredient.name}
      </li>
    );
  });

  function toggleIngredient(id, name, calories) {
    console.log(123);
    let newState = new Map(selectedIngredients);
    if (newState.get(id)) newState.delete(id);
    else newState.set(id, { id, name, calories });

    setSelectedIngredients(newState);
  }

  function addSelectedIngredients() {
    const selectedIngredientsState = Array.from(selectedIngredients.values());
    if (selectedIngredientsState.length) {
      setIngredientsIsValid(true);
      closeModal();
      setIngredients(selectedIngredientsState);
    } else {
      setIngredientsIsValid(false);
      closeModal();
      setIngredients([]);
    }
  }

  return (
    <div className="ingredients-select">
      <h1>Добавить ингредиент</h1>
      <ul className="ingredients-select__list">{renderedIngredients}</ul>
      <div className="ingredients-select__modal-controls">
        <Button basic onClick={addSelectedIngredients}>
          Добавить выбранные <br /> ингредиенты
        </Button>
        <Button
          basic
          onClick={() => openModal("addIngredient", { createIngredient })}
        >
          Создать новый <br /> ингредиент
        </Button>
      </div>
      <Icon
        onClick={closeModal}
        className="modal-close-button"
        name="close"
        size="big"
      />
    </div>
  );
};

const mapStateToProps = store => {
  return {
    ingredients: store.ingredients
  };
};

const mapDispatchToProps = dispatch => {
  return {
    closeModal: () => dispatch(closeModal()),
    openModal: (modalType, modalProps) =>
      dispatch(openModal(modalType, modalProps)),
    getIngredients: () => dispatch(getIngredients()),
    clearIngredients: () => dispatch(clearIngredients()),
    createIngredient: (name, calories) =>
      dispatch(createIngredient(name, calories))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IngredientsSelect);
