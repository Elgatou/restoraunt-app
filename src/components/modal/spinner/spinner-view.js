import React from "react";
import "./spinner.scss";

const Spinner = props => {
  return (
    <div className="spinner">
      <div className="lds-css ng-scope">
        <div className="lds-spinner">
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    </div>
  );
};

export default Spinner;
