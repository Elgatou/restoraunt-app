import React from "react";

const Confirm = ({ title, text, closeModal }) => {
  return (
    <div>
      <h2>{title}</h2>
      <p>{text}</p>
    </div>
  );
};

export default Confirm;
