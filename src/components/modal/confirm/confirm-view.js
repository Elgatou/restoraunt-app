import React from "react";
import { Button } from "semantic-ui-react";

const Confirm = ({ text, confirmAction, closeModal }) => {
  return (
    <>
      <h2>Подтвердите действие</h2>
      <p>{text}</p>
      <div className="modal-controls">
        <Button basic color="red" onClick={e => confirmAction()}>
          Да
        </Button>
        <Button basic onClick={closeModal}>
          Нет
        </Button>
      </div>
    </>
  );
};

export default Confirm;
