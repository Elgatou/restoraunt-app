import React, { useState } from "react";
import { Input, Icon, Button, Label } from "semantic-ui-react";

import "./add-ingredient.scss";
import "../modal.scss";

const AddIngredient = ({ closeModal, createIngredient }) => {
  const [ingredientIsValid, setIngredientIsValid] = useState(true);
  const [name, setName] = useState("");
  const [calories, setCalories] = useState(0);

  function validate() {
    let isValid = true;
    if (!name) {
      isValid = false;
      setIngredientIsValid(false);
    }
    return isValid;
  }

  function addIngredientHandler() {
    const isValid = validate();
    if (isValid) createIngredient(name, calories);
  }

  return (
    <div className="add-ingredient">
      <h1>Создать ингредиент</h1>
      <div className="add-ingredient__group">
        <label
          className="add-ingredient__label"
          htmlFor="ingredient-name-input"
        >
          Название
        </label>
        <Input
          id="ingredient-name-input"
          value={name}
          onChange={e => {
            setName(e.target.value);
            setIngredientIsValid(true);
          }}
        />
        {!ingredientIsValid && (
          <Label basic color="red" pointing>
            Введите название ингредиента
          </Label>
        )}
      </div>
      <div className="add-ingredient__group">
        <label
          className="add-ingredient__label"
          htmlFor="ingredient-calories-input"
        >
          Калории
        </label>
        <Input
          id="ingredient-calories-input"
          value={calories}
          onChange={e => setCalories(e.target.value)}
        />
      </div>
      <Icon
        onClick={closeModal}
        className="modal-close-button"
        name="close"
        size="big"
      />
      <Button basic onClick={() => addIngredientHandler()}>
        Ок
      </Button>
    </div>
  );
};

export default AddIngredient;
