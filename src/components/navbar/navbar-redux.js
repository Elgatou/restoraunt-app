import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { logoutAction } from "../../actions/authActions";

import "./navbar.scss";

function Navbar({ logoutAction, history }) {
  const email = localStorage.getItem("email");

  return (
    <div className="ui borderless massive menu navbar">
      <div className="header item">
        <Link to="/">Заголовок</Link>
      </div>
      <div className="right item">
        <span>
          <i>{email}</i>
        </span>
        <a
          onClick={e => logoutAction(history)}
          className="navbar__user-action"
          href="#"
        >
          Выйти
        </a>
      </div>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    logoutAction: history => dispatch(logoutAction(history))
  };
};

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(Navbar)
);
