import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "../../store/configureStrore";

import Places from "../../pages/owner/places";
import EditPlaces from "../../pages/owner/edit-place";
import editDish from "../../pages/owner/edit-dish/";
import ModalManager from "../modal/modal-manager";

import "./App.scss";
import Login from "../../pages/login-page";
import Register from "../../pages/register-page";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <ModalManager />
          <Switch>
            <Route
              path="/owner/places/:id/dishes/:dishId"
              component={editDish}
            />
            <Route path="/owner/places/:id" component={EditPlaces} />
            <Route exact path="/owner/places" component={Places} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route exact path="/" component={Login} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
