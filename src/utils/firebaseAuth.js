import axios from 'axios';
import { LOGIN_URL, REGISTER_URL, REFRESH_TOKEN_URL } from '../constants';

// if token expires then update it
export const processToken = async () => {
  if (Date.now() > localStorage.getItem('timeWhenTokenIsEnd')) {
    console.log('update token function run');
    const serverResponse = await axios.post(REFRESH_TOKEN_URL, {
      grant_type: 'refresh_token',
      refresh_token: localStorage.getItem('refreshToken'),
    });

    const timeWhenTokenIsEnd = Date.now() + (serverResponse.data.expires_in - 60) * 1000;

    localStorage.setItem('token', `Bearer ${serverResponse.data.id_token}`);
    localStorage.setItem('refreshToken', serverResponse.data.refresh_token);
    localStorage.setItem('timeWhenTokenIsEnd', timeWhenTokenIsEnd);

    return serverResponse;
  }
};

export const login = async (email, password) => {
  const serverResponse = await axios.post(LOGIN_URL, {
    email,
    password,
    returnSecureToken: true,
  });

  if (serverResponse.status === 200) {
    const timeWhenTokenIsEnd = Date.now() + (serverResponse.data.expiresIn - 60) * 1000;

    localStorage.setItem('email', serverResponse.data.email);
    localStorage.setItem('token', `Bearer ${serverResponse.data.idToken}`);
    localStorage.setItem('refreshToken', serverResponse.data.refreshToken);
    localStorage.setItem('timeWhenTokenIsEnd', timeWhenTokenIsEnd);
  }
  return serverResponse;
};

export const register = async (email, password) => {
  const serverResponse = await axios.post(REGISTER_URL, {
    email,
    password,
    returnSecureToken: true,
  });
  return serverResponse;
};

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('email');
  localStorage.removeItem('refreshToken');
  localStorage.removeItem('timeWhenTokenIsEnd');
};
