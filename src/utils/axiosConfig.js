export function getConfig() {
  return {
    headers: {
      Authorization: localStorage.getItem("token")
    }
  };
}

export function getFormDataConfig() {
  return {
    headers: {
      Authorization: localStorage.getItem("token"),
      "Content-Type": "multipart/form-data"
    }
  };
}
