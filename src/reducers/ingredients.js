import { GET_INGREDIENTS, CLEAR_INGREDIENTS } from "../actionTypes/actionTypes";

const initialState = [];

export function ingredientsReducer(state = initialState, action) {
  switch (action.type) {
    case CLEAR_INGREDIENTS: {
      return [];
    }

    case GET_INGREDIENTS: {
      return [...action.payload];
    }

    default:
      return state;
  }
}
