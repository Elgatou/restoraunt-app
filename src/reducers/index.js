import { combineReducers } from "redux";
import { commonReducer } from "./common";
import { placesReducer } from "./places";
import { dishReducer } from "./dish";
import { modalReducer } from "./modal";
import { ingredientsReducer } from "./ingredients";

export const rootReducer = combineReducers({
  common: commonReducer,
  places: placesReducer,
  dish: dishReducer,
  modal: modalReducer,
  ingredients: ingredientsReducer
});
