import { SET_EDITED_DISH, CLEAR_EDITED_DISH } from "../actionTypes/actionTypes";

const initialState = {
  editedDish: null
};

export function dishReducer(state = initialState, action) {
  switch (action.type) {
    case CLEAR_EDITED_DISH:
      return { ...state, editedDish: null };

    case SET_EDITED_DISH:
      return { ...state, editedDish: action.payload };

    default:
      return state;
  }
}
