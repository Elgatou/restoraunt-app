import { OPEN_MODAL, CLOSE_MODAL } from "../actionTypes/actionTypes";

const initialState = {
  loading: false
};

export function commonReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_MODAL:
      if (action.payload.modalType === "spinner") {
        return { ...state, loading: true };
      } else return state;

    case CLOSE_MODAL:
      if (action.payload === "loaded") {
        return { ...state, loading: false };
      } else return state;

    default:
      return state;
  }
}
