import {
  GET_PLACES,
  CLEAR_EDITED_PLACE,
  SET_EDITED_PLACE
} from "../actionTypes/actionTypes";

const initialState = {
  list: [],
  editedPlace: null
};

export function placesReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PLACES: {
      return { ...state, list: action.payload };
    }

    case CLEAR_EDITED_PLACE: {
      return { ...state, editedPlace: null };
    }

    case SET_EDITED_PLACE: {
      return { ...state, editedPlace: action.payload };
    }

    default:
      return state;
  }
}
