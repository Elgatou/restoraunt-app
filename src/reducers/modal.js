import { OPEN_MODAL, CLOSE_MODAL } from "../actionTypes/actionTypes";

const initialState = [];

export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_MODAL: {
      const { modalType, modalProps } = action.payload;
      return state.concat({ modalType, modalProps });
    }

    case CLOSE_MODAL: {
      const newState = state.slice();
      newState.pop();
      return newState;
    }

    default:
      return state;
  }
}
